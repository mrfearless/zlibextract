ZlibExtract is a static 32bit utility library for use with Masm32.

ZlibExtract encapsulates the various functions found with zlibstat.lib (Zlib static library) to add the following functionality:

- Extract all files contained within a specified zip file to a specified output folder

- Extract files that match filespec (supports '?', '*' wildcard) from a specified zip file to a specified output folder.


Pre-requisites:

Some pre-requisites are required for making full use of the ZlibExtract Library:

Zlib Static Library 

- This can be obtained via http://www.winimage.com/zLibDll/minizip.html and downloading the zlib125dll.zip file and locating the zlibstat.lib in the static32 folder.
- downloading the latest zlib.org version source and compiling a static library for use with masm32


ZlibExtract is designed for use with:

RadASM IDE 
MASM32 SDK 
 

ZlibExtract is compatible with:

Windows XP 
Windows Vista 
Windows 7 (32bit & 64Bit) 
 

zlib was written by Jean-loup Gailly (compression) and Mark Adler (decompression). Jean-loup is also the primary author/maintainer of gzip, the author of the 
comp.compression FAQ list and the former maintainer of Info-ZIP's Zip. Mark is also the author of gzip's and UnZip's main decompression routines and was the original 
author of Zip. Not surprisingly, the compression algorithm used in zlib is essentially the same as that in gzip and Zip, namely, the `deflate' method that originated 
in PKWARE's PKZIP 2.x.



Manual build options:

Assemble: X:\MASM32\BIN\ML.EXE /c /coff /Cp /nologo /I"X:\MASM32\Include" zlibextract.asm
Link    : X:\MASM32\BIN\LINK -lib zlibextract.obj /out:zlibextract.lib
