.686
.MMX
.XMM
.model flat,stdcall
option casemap:none

include windows.inc
include kernel32.inc
include user32.inc
include zlibextract.inc ; uncomment out to build lib

includelib kernel32.lib
includelib user32.lib

includelib zlib\zlibstat.lib

ZL_Extract                      PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD ; PUBLIC

ZL_GetZipInfo                   PROTO :DWORD
ZL_CreateFileName               PROTO :DWORD, :DWORD, :DWORD
ZL_CreateDirectoryPath          PROTO :DWORD
ZL_MatchFile                    PROTO :DWORD, :DWORD
ZL_JustFileName                 PROTO :DWORD, :DWORD

ZL_SetProcessInfoCallback       PROTO :DWORD ; PUBLIC
ZL_ProcessInfoCallback          PROTO :DWORD, :DWORD, :DWORD, :DWORD ; dwFileNo, dwBytesProcessed, dwStatus, lpZFI

; Taken from zlibstat.inc for internal use here
unzOpen                         PROTO :DWORD
unzGoToFirstFile                PROTO :DWORD
unzGoToNextFile                 PROTO :DWORD
unzClose                        PROTO :DWORD
unzCloseCurrentFile             PROTO :DWORD
unzGetCurrentFileInfo           PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
unzOpenCurrentFile              PROTO :DWORD
unzReadCurrentFile              PROTO :DWORD, :DWORD, :DWORD

;; structures for unzipping
;tm_unz		                    STRUCT
;    tm_sec		                DWORD ?
;    tm_min		                DWORD ?
;    tm_hour		                DWORD ?
;    tm_mday		                DWORD ?
;    tm_mon		                DWORD ?
;    tm_year		                DWORD ?
;tm_unz		                    ENDS
;;
;unz_file_info		            STRUCT
;    version			            DWORD ?
;    version_needed	            DWORD ?
;    flag			            DWORD ? 
;    compression_method	        DWORD ?
;    dosDate				        DWORD ?
;    crc					        DWORD ?
;    compressed_size		        DWORD ?
;    uncompressed_size	        DWORD ?
;    size_filename		        DWORD ?
;    size_file_extra		        DWORD ?
;    size_file_comment	        DWORD ?
;    disk_num_start		        DWORD ?
;    internal_fa			        DWORD ?
;    external_fa			        DWORD ? ;00002010 DOS Directory - 00002020 archive
;    tmu_date			        tm_unz <>
;unz_file_info		            ENDS

;ZL_ZIPARCHIVEINFO               STRUCT
;    dwTotalFiles                DD 0 ; total files in zip archive
;    dwTotalFolders              DD 0 ; total folders in zip archive
;    dwTotalFilesCompressedSize  DD 0 ; total sizes of all files (Compressed) in zip archive
;    dwTotalFilesUncompressedSize DD 0; total sizes of all files (uncompressed) in zip archive
;ZL_ZIPARCHIVEINFO               ENDS
;
;ZL_PROCESSFILEINFO              STRUCT
;    dwFileNo                    DD 0 ; file number processed
;    szFullPathFilename          DB MAX_PATH dup (0) ; full path and filename
;    szFileName                  DB MAX_PATH dup (0) ; just filename and extension
;    dwBytesProcessed            DD 0 ; bytes processed (written) 
;    dwPercentageProcessed       DD 0 ; calculated percentage
;ZL_PROCESSFILEINFO              ENDS
;
;; Structure of internal file info time (sub struct of ZL_FILEINFO) passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
;ZL_FILETIMEINFOX                 STRUCT
;    tm_sec		                DWORD ?
;    tm_min		                DWORD ?
;    tm_hour		                DWORD ?
;    tm_mday		                DWORD ?
;    tm_mon		                DWORD ?
;    tm_year		                DWORD ?
;ZL_FILETIMEINFOX		            ENDS
;
;; Structure of internal file info passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
;ZL_FILEINFOX  		            STRUCT
;    version			            DD ?
;    version_needed	            DD ?
;    flag			            DD ? 
;    compression_method	        DD ?
;    dosDate				        DD ?
;    crc					        DD ?
;    compressed_size		        DD ?
;    uncompressed_size	        DD ?
;    size_filename		        DD ?
;    size_file_extra		        DD ?
;    size_file_comment	        DD ?
;    disk_num_start		        DD ?
;    internal_fa			        DD ?
;    external_fa			        DD ? 
;    tmu_date			        ZL_FILETIMEINFOX <>
;ZL_FILEINFOX		                ENDS

.CONST
ZLPROCESSBLOCKSIZE              EQU 2048d

; ZLE_MODE constants. ZL_Extract values for dwOptions paramter:
ZLE_MODE_EXTRACT                EQU 0
ZLE_MODE_LIST                   EQU 1
ZLE_MODE_INFO                   EQU 2

; ZL_Extract Status, Error & Return Values
ZLE_STATUS_BEGIN_PROCESSING     EQU  5 ; Start processing of a file in the zip archive: extracting/listing etc
ZLE_STATUS_PROCESSING           EQU  4 ; Actual processing of file. Passed param has details of file, bytes etc
ZLE_STATUS_FINISH_PROCESSING    EQU  3 ; Finished processing of file in zip archive.
ZLE_STATUS_LISTING_FILES        EQU  2 ; Listing files only, no extraction
ZLE_STATUS_ARCHIVE_INFO         EQU  1 ; Info about archive only, no extraction
ZLE_STATUS_SUCCESS              EQU  0 ; Extracted file(s) succesfully or finished listing files
ZLE_ERR_NO_ZIP                  EQU -1 ; No zip archive specified
ZLE_ERR_INVALID_ZIP             EQU -2 ; Invalid zip archive
ZLE_ERR_INTERNAL_INFO           EQU -3 ; Failed to get internal zip file info
ZLE_ERR_OPEN_FILE_FAILURE       EQU -4 ; Failed to open internal zip file
ZLE_ERR_ALLOC_MEMORY            EQU -5 ; Failed to allocate memory for unzip operation
ZLE_ERR_READ_DATA_FAILURE       EQU -6 ; Failed to read internal zip file
ZLE_ERR_CREATE_FILE_FAILURE     EQU -7 ; failed to create extracted file
ZLE_ERR_WRITE_DATA_FAILURE      EQU -8 ; Failed to write data to extracted file
ZLE_ERR_NO_MATCHING_FILES_FOUND EQU -9 ; No files matching filespec where found

.DATA
szAll                           db "*.*",0
szBackSlash                     db "\",0
ZL_ProcessInfoCallbackProc      DD 0 ; holds address of procedure (as set by user) to receive call back info whilst extract/list operation is running

ZLProcessInfo                   ZL_PROCESSFILEINFO <>
ZLZipArchiveInfo                ZL_ZIPARCHIVEINFO <>


.CODE

;**************************************************************************
; Extract files from zip file to a specific folder or current folder
;**************************************************************************
ZL_Extract  PROC PUBLIC USES ESI  dwOptions:DWORD, lpszZipFile:DWORD, lpszFileSpec:DWORD, lpszOutputFolder:DWORD, lpszExcludeFileSpec:DWORD
    LOCAL hZipFile:DWORD
    LOCAL hFile:DWORD
    LOCAL ZFI:ZL_FILEINFO
    LOCAL filename_inzip[MAX_PATH]:BYTE
    LOCAL szJustFilename[MAX_PATH]:BYTE
    LOCAL fullextractedfilepath[MAX_PATH]:BYTE
    LOCAL hMem:DWORD
    LOCAL BytesWritten:DWORD
    LOCAL fit:FILETIME
    LOCAL foundfiles:DWORD
    LOCAL FileNo:DWORD 
    LOCAL zposition:DWORD
    LOCAL FilesProcessed:DWORD

    LOCAL ZLZEMode:DWORD ; extract or list
    
    Invoke RtlZeroMemory, Addr ZFI, SIZEOF ZL_FILEINFO
    Invoke RtlZeroMemory, Addr filename_inzip, SIZEOF filename_inzip
    Invoke RtlZeroMemory, Addr fullextractedfilepath, SIZEOF fullextractedfilepath
        
    .IF lpszZipFile == NULL
        Invoke ZL_ProcessInfoCallback, 0, 0, ZLE_ERR_NO_ZIP, Addr ZFI
        mov ebx, 0
        mov eax, -1 ; No zip archive specified
        ret
    .ENDIF
    
    IFDEF DEBUG32
        PrintText 'unzOpen'
        PrintStringByAddr lpszZipFile
    ENDIF
    Invoke unzOpen, lpszZipFile
	.IF eax == FALSE
	    Invoke ZL_ProcessInfoCallback, 0, 0, ZLE_ERR_INVALID_ZIP, Addr ZFI
        mov ebx, 0
        mov eax, -2 ; Invalid zip archive
        ret
    .ENDIF
    mov hZipFile, eax
    mov foundfiles, FALSE
    mov FileNo, 0
    mov FilesProcessed, 0
    
    ; Get zip info
    Invoke ZL_GetZipInfo, hZipFile ;, Addr dwTotalZipFiles, Addr dwTotalZipFolders, Addr dwTotalZipFilesSize
    Invoke ZL_ProcessInfoCallback, 0, 0, ZLE_STATUS_ARCHIVE_INFO, Addr ZFI
    .IF dwOptions == ZLE_MODE_INFO
        mov ebx, 0
        mov eax, 0
        ret
    .ENDIF
    
    IFDEF DEBUG32
        PrintText 'unzGoToFirstFile'
    ENDIF
    Invoke unzGoToFirstFile, hZipFile
    ;PrintDec eax
    .WHILE !eax    
        IFDEF DEBUG32
            PrintText 'unzGetCurrentFileInfo'
        ENDIF
        Invoke unzGetCurrentFileInfo, hZipFile, Addr ZFI, Addr filename_inzip, SIZEOF filename_inzip, NULL, 0, NULL, 0
        .IF !eax
            lea esi, filename_inzip
            invoke lstrlen, Addr filename_inzip
            add esi, eax
            dec esi
            movzx eax, byte ptr [esi]
            .IF al == '\' || al == '/'
            ;mov eax, ZFI.external_fa
            ;PrintDec ZFI.external_fa
            ;and eax, 0000000FFh ; mask to see if external file attributes indicate a folder
            ;PrintDec eax
            ;.IF eax == 10h || eax == 30h ; ZFI.external_fa == 10h || ZFI.external_fa == 2010h;; folder
                    IFDEF DEBUG32
                        ;PrintDec ZFI.external_fa
                        lea esi, filename_inzip
                        PrintStringByAddr esi
                        PrintText 'Just a path listed in zip file so just create that folder' ; could be empty folder we need to create
                    ENDIF            
                .IF dwOptions == ZLE_MODE_EXTRACT
                    Invoke ZL_CreateFileName, Addr fullextractedfilepath, lpszOutputFolder, Addr filename_inzip
                    Invoke ZL_CreateDirectoryPath, Addr fullextractedfilepath

                .ENDIF
				Invoke unzGoToNextFile, hZipFile    
            .ELSE
                Inc FileNo
                Invoke ZL_MatchFile, Addr filename_inzip, lpszFileSpec
                .IF eax == TRUE ; we matched filespec to internal filename so go get em'
                    mov foundfiles, TRUE
                    .IF dwOptions == ZLE_MODE_EXTRACT
                        IFDEF DEBUG32
                            PrintText 'unzOpenCurrentFile'
                        ENDIF     
                        Invoke unzOpenCurrentFile, hZipFile
                        .IF !eax ;!= FALSE
                            IFDEF DEBUG32
                                PrintText 'GlobalAlloc'
                            ENDIF
            				.IF ZFI.uncompressed_size != 0
            				    Invoke GlobalAlloc, GMEM_FIXED, ZFI.uncompressed_size
            				.ELSE
            				    Invoke GlobalAlloc, GMEM_FIXED, 8h ; just alloc something for the time being
            				.ENDIF
            				.IF eax != NULL
            					mov hMem, eax
            					IFDEF DEBUG32
            					    PrintText 'unzReadCurrentFile'
            					ENDIF
            					Invoke unzReadCurrentFile, hZipFile, hMem, ZFI.uncompressed_size
            					;IFDEF DEBUG32
            					;    PrintDec eax
            					;    PrintDec ZFI.uncompressed_size
            					;ENDIF
            					.If eax == ZFI.uncompressed_size
            					    Invoke ZL_CreateFileName, Addr fullextractedfilepath, lpszOutputFolder, Addr filename_inzip        
            					    Invoke ZL_CreateDirectoryPath, Addr fullextractedfilepath 
            						IFDEF DEBUG32
            						    PrintText 'CreateFile'
                				        ;lea esi, fullextractedfilepath
                				        ;PrintStringByAddr esi						    
            						ENDIF
            						Invoke CreateFile, Addr fullextractedfilepath, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL
            						;Invoke unzCreateFile,addr filename_inzip
            						.IF eax != INVALID_HANDLE_VALUE
            							mov	hFile, eax
            							IFDEF DEBUG32
            							    PrintText 'WriteFile'
            							ENDIF
            							
            							; Set info in ZLProcessInfo Struct ; only set it here once before ZLE_STATUS_BEGIN_PROCESSING instead of each time calling ZL_ProcessInfoCallback
                                        
                                        lea ebx, ZLProcessInfo
                                        lea esi, [ebx].ZL_PROCESSFILEINFO.szFullPathFilename
                                        Invoke lstrcpy, esi, Addr fullextractedfilepath
                                        Invoke ZL_JustFileName, Addr fullextractedfilepath, Addr szJustFilename
                                        
                                        lea ebx, ZLProcessInfo
                                        lea esi, [ebx].ZL_PROCESSFILEINFO.szFileName
                                        Invoke lstrcpy, esi, Addr szJustFilename
    
            							IFDEF DEBUG32
            							    PrintText 'ZL_ProcessInfoCallback'
            							ENDIF        							
            							Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_STATUS_BEGIN_PROCESSING, Addr ZFI
            							
            							.IF ZFI.uncompressed_size != 0
            							    
            							    mov zposition, 0
            							    Invoke ZL_ProcessInfoCallback, FileNo, zposition, ZLE_STATUS_PROCESSING, Addr ZFI
            							    mov eax, 0
    
            							    .WHILE eax < ZFI.uncompressed_size
            							        mov eax, zposition
            							        add eax, ZLPROCESSBLOCKSIZE
            							        .IF eax <= ZFI.uncompressed_size
            							            mov esi, hMem
            							            add esi, zposition
            							            Invoke WriteFile, hFile, esi, ZLPROCESSBLOCKSIZE, Addr BytesWritten, NULL
                        							.IF eax == 0
                        								Invoke GlobalFree, hMem
                        								Invoke CloseHandle, hFile
                        						        Invoke unzCloseCurrentFile, hZipFile
                        						        Invoke unzClose, hZipFile
                        						        Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_WRITE_DATA_FAILURE, Addr ZFI
                        						        mov ebx, FilesProcessed
                        							    mov eax, -8 ; Failed to write data to extracted file
                        							    ret
                        							.ENDIF
    
    							                    add zposition, ZLPROCESSBLOCKSIZE
    							                    Invoke ZL_ProcessInfoCallback, FileNo, zposition, ZLE_STATUS_PROCESSING, Addr ZFI
            							        .ELSE
            							            mov ebx, ZFI.uncompressed_size
            							            sub ebx, zposition
            							            mov esi, hMem
            							            add esi, zposition
            							            Invoke WriteFile, hFile, esi, ebx, Addr BytesWritten, NULL
                        							.IF eax == 0
                        								Invoke GlobalFree, hMem
                        								Invoke CloseHandle, hFile
                        						        Invoke unzCloseCurrentFile, hZipFile
                        						        Invoke unzClose, hZipFile
                        						        Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_WRITE_DATA_FAILURE, Addr ZFI
                        						        mov ebx, FilesProcessed
                        							    mov eax, -8 ; Failed to write data to extracted file
                        							    ret
                        							.ENDIF
                  							
            							            mov eax, ZFI.uncompressed_size
            							            mov zposition, eax
            							            Invoke ZL_ProcessInfoCallback, FileNo, zposition, ZLE_STATUS_PROCESSING, Addr ZFI
            							        .ENDIF
            							        mov eax, zposition
            							        ;PrintDec eax
            							    .ENDW
            							    Inc FilesProcessed
            							    ; todo, write out in a blocksize (4096) till = ZFI.uncompressed_size and call callback function to allow user to display info
            							    ; position in memory hmem to write from each time, if ((position + blocksize) <= ZFI.uncompressed_size) write out blocksize
            							    ; otherwise write out (ZFI.uncompressed_size - position) to end. Add position to hmem each loop to get next block
            							    ; maybe todo, calc percentage written.
    ;        							
    ;            							Invoke WriteFile, hFile, hMem, ZFI.uncompressed_size, Addr BytesWritten, NULL
    ;            							.IF eax == 0
    ;            								Invoke GlobalFree, hMem
    ;            								Invoke CloseHandle, hFile
    ;            						        Invoke unzCloseCurrentFile, hZipFile
    ;            						        Invoke unzClose, hZipFile
    ;            						        Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_WRITE_DATA_FAILURE, Addr ZFI
    ;            							    mov eax, -8 ; Failed to write data to extracted file
    ;            							    ret
    ;            							.ENDIF
                						.ENDIF	
            							IFDEF DEBUG32
            							    PrintText 'Set File Date & Close File'
            							ENDIF							
            							mov ecx, ZFI.dosDate
            							movzx edx,cx
            							shr ecx,16
            							Invoke DosDateTimeToFileTime, ecx, edx, addr fit
            							Invoke SetFileTime, hFile, addr fit, addr fit, addr fit
            							Invoke CloseHandle, hFile
            							
            							Invoke ZL_ProcessInfoCallback, FileNo, ZFI.uncompressed_size, ZLE_STATUS_FINISH_PROCESSING, Addr ZFI
            							
            						.ELSE
            						    Invoke GlobalFree, hMem
            						    Invoke unzCloseCurrentFile, hZipFile
            						    Invoke unzClose, hZipFile
            						    Invoke ZL_ProcessInfoCallback, FileNo, ZFI.uncompressed_size, ZLE_ERR_CREATE_FILE_FAILURE, Addr ZFI
            						    mov ebx, FilesProcessed
            						    mov eax, -7 ; failed to create extracted file
            						    ret
            						.ENDIF
            	    				IFDEF DEBUG32
            						    PrintText 'GlobalFree: unzCloseCurrentFile: unzGoToNextFile'
            						ENDIF
            						Invoke GlobalFree, hMem
            						Invoke unzCloseCurrentFile, hZipFile
            						Invoke unzGoToNextFile, hZipFile
            						; loop again to get next file if there is one						
            				    .ELSE
            						Invoke GlobalFree, hMem
            						Invoke unzCloseCurrentFile, hZipFile
            						Invoke unzClose, hZipFile
            						Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_READ_DATA_FAILURE, Addr ZFI
            						mov ebx, FilesProcessed
            				        mov eax, -6 ; Failed to read internal zip file
            				        ret
            				    .ENDIF
            				.ELSE
            				    Invoke unzCloseCurrentFile, hZipFile
            				    Invoke unzClose, hZipFile
            				    Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_ALLOC_MEMORY, Addr ZFI
            				    mov ebx, FilesProcessed
            				    mov eax, -5 ; Failed to allocate memory for unzip operation
            				    ret
            				.ENDIF
            			.ELSE
            			    Invoke unzClose, hZipFile
            			    Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_OPEN_FILE_FAILURE, Addr ZFI
            			    mov ebx, FilesProcessed
            			    mov eax, -4 ; Failed to open internal zip file
            			    ret
                        .ENDIF
                    .ELSEIF dwOptions == ZLE_MODE_LIST
                        ; add stuff to ZLProcessInfo struct for the list mode to process
                        ;Invoke ZL_CreateFileName, Addr fullextractedfilepath, lpszOutputFolder, Addr filename_inzip

                        lea ebx, ZLProcessInfo
                        lea esi, [ebx].ZL_PROCESSFILEINFO.szFullPathFilename
                        Invoke lstrcpy, esi, Addr filename_inzip
                        Invoke ZL_JustFileName, Addr filename_inzip, Addr szJustFilename
                        
                        lea ebx, ZLProcessInfo
                        lea esi, [ebx].ZL_PROCESSFILEINFO.szFileName
                        Invoke lstrcpy, esi, Addr szJustFilename                    
                        Invoke ZL_ProcessInfoCallback, FileNo, ZFI.uncompressed_size, ZLE_STATUS_LISTING_FILES, Addr ZFI
                        Inc FilesProcessed
                        Invoke unzGoToNextFile, hZipFile
                    .ELSE ; add other options maybe?
                        Invoke unzGoToNextFile, hZipFile
                    .ENDIF
                .ELSE
                    Invoke unzGoToNextFile, hZipFile
                .ENDIF
            .ENDIF  ; if just a path we loop again  
        .ELSE
            Invoke unzClose, hZipFile
            Invoke ZL_ProcessInfoCallback, FileNo, 0, ZLE_ERR_INTERNAL_INFO, Addr ZFI
            mov ebx, FilesProcessed
            mov eax, -3 ; Failed to get internal zip file info
            ret
        .ENDIF
    .ENDW
    IFDEF DEBUG32
        PrintText 'File Unzipped Succesfully'
    ENDIF
    Invoke unzClose, hZipFile
    .IF foundfiles == TRUE
        Invoke ZL_ProcessInfoCallback, 0, 0, ZLE_STATUS_SUCCESS, Addr ZFI
        mov ebx, FilesProcessed
        mov eax, ZLE_STATUS_SUCCESS ; 0 ; all ok
    .ELSE
        Invoke ZL_ProcessInfoCallback, 0, 0, ZLE_ERR_NO_MATCHING_FILES_FOUND, Addr ZFI
        mov ebx, 0
        mov eax, ZLE_ERR_NO_MATCHING_FILES_FOUND ;-9
    .ENDIF          
    ret

ZL_Extract endp


;**************************************************************************
; Sets user defined callback proc - Set to NULL to disable
;**************************************************************************
ZL_SetProcessInfoCallback PROC PUBLIC lpdwCallbackProc:DWORD
    mov eax, lpdwCallbackProc
    mov ZL_ProcessInfoCallbackProc, eax
    xor eax, eax
    ret
ZL_SetProcessInfoCallback endp

;**************************************************************************
; Returns true if internalfilename matches the filespec or false otherwise
;**************************************************************************
ZL_MatchFile PROC PRIVATE USES EDI ESI lpszInternalFileName:DWORD, lpszFileSpec:DWORD
    LOCAL szInternalFileName[MAX_PATH]:BYTE
    LOCAL szFileSpec[MAX_PATH]:BYTE
    LOCAL lenInternalFileName:DWORD
    LOCAL lenFileSpec:DWORD
    LOCAL position:DWORD
    LOCAL matchflag:DWORD
    LOCAL matchchar:BYTE
    
   .IF lpszFileSpec == NULL
        mov eax, TRUE
        ret
    .ENDIF
    
    ;mov esi, lpszFileSpec
    ;mov eax, [esi]
    Invoke lstrcmp, Addr szAll, lpszFileSpec
    ;PrintStringByAddr lpszFileSpec
    .IF eax == 0 ; '*.*'
        ;IFDEF DEBUG32
        ;    PrintText 'Found *.*'
        ;ENDIF
        mov eax, TRUE
        ret
    .ENDIF    
    
    ; convert both to upper case
    Invoke ZL_JustFileName, lpszInternalFileName, Addr szInternalFileName
    Invoke lstrcpy, Addr szFileSpec, lpszFileSpec
    Invoke CharUpperBuff, Addr szInternalFileName, MAX_PATH
    Invoke CharUpperBuff, Addr szFileSpec, MAX_PATH
    Invoke lstrlen, Addr szInternalFileName
    mov lenInternalFileName, eax
    Invoke lstrlen, Addr szFileSpec
    mov lenFileSpec, eax
    
    lea esi, szInternalFileName
    lea edi, szFileSpec
    ;IFDEF DEBUG32
    ;    PrintText 'ZL_MatchFile'
    ;    PrintStringByAddr esi
    ;    PrintStringByAddr edi
    ;ENDIF

    mov matchflag, FALSE
    mov position, 0
    mov eax, 0
    .WHILE eax <= lenInternalFileName
        movzx eax, byte ptr [esi]
        movzx ebx, byte ptr [edi]
        .IF bl == al
            inc position
            inc esi
            inc edi
            mov matchflag, TRUE
            ;PrintStringByAddr esi
        .ELSE
            .IF bl == '?'
                ;IFDEF DEBUG32
                ;    PrintText 'Found ?'
                ;ENDIF            
                inc position
                inc esi
                inc edi
                mov matchflag, TRUE
            .ELSEIF bl == '*'
                ;IFDEF DEBUG32
                ;    PrintText 'Found *'
               ; ENDIF
                inc position
                inc esi
                inc edi
                movzx edx, byte ptr [edi] ; next character after asterisk to match up to
                mov matchchar, dl
                .IF matchchar == 0 ; null byte at end of string we assume rest of string is matched so just set matchflag to true and break out of loop
                    mov eax, TRUE
                    ret
                .ENDIF
                ; else we check for next char to match upto and start the loop again to check from there.
                ;PrintDec matchchar
                mov matchflag, FALSE
                mov eax, position
                .WHILE eax <= lenInternalFileName
                    ;PrintStringByAddr esi
                    movzx eax, byte ptr [esi]
                    
                    .IF al == matchchar
                        ;IFDEF DEBUG32
                        ;    PrintText 'Found next char to check for'
                        ;    PrintDec eax
                        ;    PrintDec edx
                        ;ENDIF
                        mov matchflag, TRUE
                        .BREAK
                    .ENDIF
                    inc position
                    inc esi
                    ;inc edi
                    mov eax, position
                .ENDW
            .ELSEIF al == 0h || bl == 0h
                ;IFDEF DEBUG32
                ;    PrintText 'Found null'
                ;    PrintDec eax
                ;    PrintDec ebx
                ;ENDIF            
                mov matchflag, FALSE
                .BREAK
            .ELSE
                IFDEF DEBUG32
                    PrintText 'No match'
                ENDIF
                mov matchflag, FALSE
                .BREAK
            .ENDIF
        .ENDIF
        mov eax, position
    .ENDW
    ;IFDEF DEBUG32
    ;    PrintDec matchflag
    ;ENDIF
    mov eax, matchflag
    
    ret
ZL_MatchFile ENDP

;**************************************************************************
; Create full path filename for extracted file
;**************************************************************************
ZL_CreateFileName PROC PRIVATE USES EDI ESI lpszFullFolderPathFileName:DWORD, lpszBaseOutputFolder:DWORD, lpszBaseOutputFile:DWORD

    LOCAL lenOutputFolder:DWORD
    LOCAL lenOutputFile:DWORD
    
    ;PrintStringByAddr lpszBaseOutputFolder
	.IF lpszBaseOutputFolder != NULL
	    Invoke lstrlen, lpszBaseOutputFolder
	    mov lenOutputFolder, eax
	   ; PrintDec lenOutputFolder
	    
	    .IF lenOutputFolder != 0

	        Invoke lstrcpy, lpszFullFolderPathFileName, lpszBaseOutputFolder
		    mov esi, lpszBaseOutputFolder
		    add esi, lenOutputFolder
		    dec esi
		    movzx eax, byte ptr [esi]
		    .IF al != '\' && al != '/'
		        Invoke lstrcat, lpszFullFolderPathFileName, Addr szBackSlash
    	    .ENDIF
    	    ;IFDEF DEBUG32
    	    ;    mov esi, lpszFullFolderPathFileName
    	    ;    PrintStringByAddr esi
    	    ;ENDIF
	    .ELSE
	        Invoke GetCurrentDirectory, MAX_PATH, lpszFullFolderPathFileName
	        Invoke lstrcat, lpszFullFolderPathFileName, Addr szBackSlash
    	    ;IFDEF DEBUG32
    	     ;   mov esi, lpszFullFolderPathFileName
    	    ;    PrintStringByAddr esi
    	    ;ENDIF					        
	    .ENDIF
	.ELSE
	    Invoke GetCurrentDirectory, MAX_PATH, lpszFullFolderPathFileName
	    Invoke lstrcat, lpszFullFolderPathFileName, Addr szBackSlash
	    ;IFDEF DEBUG32
    	;    mov esi, lpszFullFolderPathFileName
    	;    PrintStringByAddr esi
        ;ENDIF						    
	.ENDIF
	IFDEF DEBUG32
	    ;PrintText 'filename_inzip:'
	    ;mov esi, lpszBaseOutputFile
	    ;PrintStringByAddr esi
	ENDIF
    Invoke lstrcat, lpszFullFolderPathFileName, lpszBaseOutputFile
    ;IFDEF DEBUG32
    ;    PrintText 'fullextractedfilepath:'
    ;    mov esi, lpszFullFolderPathFileName
    ;    PrintStringByAddr esi
    ;ENDIF

    ret
ZL_CreateFileName ENDP


;**************************************************************************
; Create each directory in turn from a folderpath
;**************************************************************************
ZL_CreateDirectoryPath PROC PRIVATE USES EDI ESI szFolderPath:DWORD
	LOCAL LastBackSlashPos:DWORD
	LOCAL LastFullStopPos:DWORD
	LOCAL LoopCounter:DWORD

    mov LastFullStopPos, 0      ; initialize to 0
    mov LastBackSlashPos, 0     ; initialize to 0
    mov LoopCounter, 0
    mov esi, szFolderPath
    
@@:
    movzx eax, byte ptr [esi]   ; read byte from address in esi
	
	cmp al, 0                   ; test for zero    
	je ExitLoop                 ; if null byte then we exit our loop   
	
	cmp al, "."                 ; test for .
	jne CheckBackslash	        ; if found we record position for later use
	mov ecx, LoopCounter
    mov LastFullStopPos, ecx
    
CheckBackslash:
    cmp al, "\"                 ; test for "\" - means we hit an end of a folder whilst looping through each byte
	jne CheckForwardslash       ; jump over if its not, otherwise create folder path up to this point
	jmp Foundslash              ; otherwise found slash char so continue on over next check
	
CheckForwardslash:
    cmp al, "/"                 ; test for "/" - means we hit an end of a folder whilst looping through each byte
    jne LoopAgain               ; jump over if its not, otherwise create folder path up to this point

Foundslash:    	
	mov ecx, LoopCounter
	mov LastBackSlashPos, ecx   ; record backslash pos

	mov byte ptr [esi], 0       ; write null byte to address in esi as esi contains our path up to this point
    Invoke GetFileAttributes, szFolderPath
    .IF eax != FILE_ATTRIBUTE_DIRECTORY
        Invoke CreateDirectory, szFolderPath, NULL
    .ENDIF	
	mov byte ptr [esi], '\'       ; restore \ byte to address in esi as esi contains our path up to this point
	
LoopAgain:
 	inc esi
 	inc LoopCounter
    jmp @B   

ExitLoop:

    mov eax, LastFullStopPos
    .IF eax < LastBackSlashPos ; we have one final directory to possibly create, else we got a filename with an extention
        Invoke GetFileAttributes, szFolderPath
        .IF eax != FILE_ATTRIBUTE_DIRECTORY
            Invoke CreateDirectory, szFolderPath, NULL
            
        .ENDIF	        
    .ENDIF
    xor eax, eax
    ret	
ZL_CreateDirectoryPath endp


;**************************************************************************
; Strip path name to just filename With extention
;**************************************************************************
ZL_JustFileName PROC PRIVATE lpszFilePathName:DWORD, lpszFileName:DWORD

	LOCAL LenFilePathName:DWORD
	LOCAL nPosition:DWORD
	;LOCAL cLetter[4]:bYTE
	
	Invoke lstrlen, lpszFilePathName
	mov LenFilePathName, eax
	mov nPosition, eax
	
	.IF LenFilePathName == 0
		mov byte ptr [edi], 0
		ret
	.endif
	
	mov esi, lpszFilePathName
	add esi, eax
	
	mov eax, nPosition
	.WHILE eax != 0
		movzx eax, byte ptr [esi]
		;mov byte ptr [cLetter], al
		;mov byte ptr [cLetter+1], 0
		;PrintString cLetter
		.IF al == '\' || al == ':' || al == '/'
			;PrintText 'Found / or :'
			inc esi
			.BREAK
		.endif
		dec esi
		dec nPosition
		mov eax, nPosition
	.ENDW
	mov edi, lpszFileName
	;PrintDec nPosition
	mov eax, nPosition
	.WHILE eax != LenFilePathName
		movzx eax, byte ptr [esi]
		;.IF al == 0h ;'.' ; found our null - so stop here
		;    mov byte ptr [edi], 0h
		;    .BREAK
		;.endif
		mov byte ptr [edi], al
		inc edi
		inc esi
		inc nPosition
		mov eax, nPosition
	.ENDW
	mov byte ptr [edi], 0h
	ret
ZL_JustFileName endp


;**************************************************************************
; Retrieve info from zip file on how many files and folder entries it has
;**************************************************************************
ZL_GetZipInfo PROC hZipFile:DWORD
    LOCAL ZFI:ZL_FILEINFO
    LOCAL filename_inzip[MAX_PATH]:BYTE
    LOCAL FileCount:DWORD
    LOCAL FolderCount:DWORD
    LOCAL FilesCompressedSize:DWORD
    LOCAL FilesUncompressedSize:DWORD
    
    mov FileCount, 0
    mov FolderCount, 0
    mov FilesCompressedSize, 0
    mov FilesUncompressedSize, 0
       
    Invoke unzGoToFirstFile, hZipFile
    .WHILE !eax
        Invoke unzGetCurrentFileInfo, hZipFile, Addr ZFI, Addr filename_inzip, SIZEOF filename_inzip, NULL, 0, NULL, 0
        .IF ZFI.external_fa == 10h || ZFI.external_fa == 2010h; folder
            Inc FolderCount
        .ELSE
            mov eax, ZFI.compressed_size
            add FilesCompressedSize, eax
            mov eax, ZFI.uncompressed_size
            add FilesUncompressedSize, eax
            Inc FileCount
        .ENDIF
        Invoke unzGoToNextFile, hZipFile
    .ENDW
    
    lea ebx, ZLZipArchiveInfo
    mov eax, FileCount
    mov [ebx].ZL_ZIPARCHIVEINFO.dwTotalFiles, eax
    
    mov eax, FolderCount
    mov [ebx].ZL_ZIPARCHIVEINFO.dwTotalFolders, eax

    mov eax, FilesCompressedSize
    mov [ebx].ZL_ZIPARCHIVEINFO.dwTotalFilesCompressedSize, eax
    
    mov eax, FilesUncompressedSize
    mov [ebx].ZL_ZIPARCHIVEINFO.dwTotalFilesUncompressedSize, eax
    mov eax, TRUE
    ret

ZL_GetZipInfo endp


;**************************************************************************
; Callback function to allow user to display info whilst extracting files
;**************************************************************************
ZL_ProcessInfoCallback PROC PRIVATE dwFileNo:DWORD, dwBytesProcessed:DWORD, dwStatus:DWORD, lpZFI:DWORD

    .IF ZL_ProcessInfoCallbackProc != NULL
        lea ebx, ZLProcessInfo
        mov eax, dwFileNo
        mov [ebx].ZL_PROCESSFILEINFO.dwFileNo, eax
        
        lea ebx, ZLProcessInfo
        .IF dwStatus == ZLE_STATUS_LISTING_FILES || dwStatus == ZLE_STATUS_FINISH_PROCESSING
            mov eax, lpZFI.ZL_FILEINFO.uncompressed_size
            mov [ebx].ZL_PROCESSFILEINFO.dwBytesProcessed, eax
            mov eax, 100
            mov [ebx].ZL_PROCESSFILEINFO.dwPercentageProcessed, eax
        .ELSE
            .IF dwStatus == ZLE_STATUS_BEGIN_PROCESSING
                lea ebx, ZLProcessInfo
                mov eax, 0
                mov [ebx].ZL_PROCESSFILEINFO.dwBytesProcessed, eax
                mov eax, 0
                mov [ebx].ZL_PROCESSFILEINFO.dwPercentageProcessed, eax        
            
            .ELSEIF dwStatus == ZLE_STATUS_PROCESSING
                ; todo calc percentage
                mov ebx, lpZFI
                mov eax, [ebx].ZL_FILEINFO.uncompressed_size
                .IF eax != 0 ;dwBytesProcessed != 0 && eax != 0
                    ;IFDEF DEBUG32
                    ;    PrintText 'Calc Percentage'
                    ;ENDIF
                    lea ebx, ZLProcessInfo
                    mov eax, dwBytesProcessed
                    mov [ebx].ZL_PROCESSFILEINFO.dwBytesProcessed, eax
                    mov ebx, 100d
                    mul ebx
                    xor edx, edx
                    mov ebx, lpZFI
                    mov ecx, [ebx].ZL_FILEINFO.uncompressed_size
                    div ecx
                    lea ebx, ZLProcessInfo
                    mov [ebx].ZL_PROCESSFILEINFO.dwPercentageProcessed, eax
                    ;PrintDec eax
                .ELSE
                    lea ebx, ZLProcessInfo
                    mov eax, 0
                    mov [ebx].ZL_PROCESSFILEINFO.dwBytesProcessed, eax
                    mov eax, 0
                    mov [ebx].ZL_PROCESSFILEINFO.dwPercentageProcessed, eax
                .ENDIF
            .ENDIF 
        .ENDIF
        
        ; push params in reverse order and call user defined callback
        push dwStatus
        push lpZFI        
        lea eax, ZLProcessInfo
        push eax
        lea eax, ZLZipArchiveInfo
        push eax        
        call ZL_ProcessInfoCallbackProc
    .ENDIF
    xor eax, eax
    ret
ZL_ProcessInfoCallback ENDP

END
