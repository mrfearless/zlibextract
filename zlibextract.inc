;-----------------------------------------------------------------------------------------------------------------------------------------------------------------
; ZLIBEXTRACT.LIB v1.1 - fearless - www.LetTheLight.in - 2013
;
; Utility library for ZLIBSTAT.LIB to allow unzipping file or files from compressed zip archives
;
; Requires ZLIBSTAT.LIB - Static version of Zlib available from: 
; http://www.winimage.com/zLibDll/minizip.html
; zlibstat.lib (v1.2.5) is included in the zlib125dll.zip package under the static32 folder.
;
;-----------------------------------------------------------------------------------------------------------------------------------------------------------------

;-----------------------------------------------------------------------------------------------------------------------------------------------------------------
;
; ZL_Extract                     Extract or list files from a zip archive that match filename or filespec and output them to a specified folder.
;
; Parameters:
;
;   - dwOptions                  Mode of operation as defined by ZL_MODE constants: Extract, List, Info. 
;                                If dwOptions is NULL then ZLE_MODE_EXTRACT is assumed.
;   - lpszZipFile                Pointer to zero terminated string containing the filename and path of zip archive to open.
;                                lpszZipFile is required, cannot be NULL or it will return ZLE_ERR_NO_ZIP. An invalid zip file will return ZLE_ERR_INVALID_ZIP
;   - lpszFileSpec               Pointer to zero terminated string containing the filename or file spec to match to include for the current mode of operation. 
;                                lpszFileSpec parameter is optional. If NULL assumes all files are to be included (*.*)
;                                Supports the use of wildcards ? and * in the filename.
;   - lpszOutputFolder           Pointer to zero terminated string containing the output folder path to extract files to. 
;                                lpszOutputFolder parameter is optional. If lpszOutputFolder is NULL the current folder is assumed.
;   - lpszExcludeFileSpec        Pointer to zero terminated string containing the filename or file spec to match to exclude for the current mode of operation.
;                                lpszExcludeFileSpec parameter is optional. If NULL assumes no files are to be excluded.
;                                Supports the use of wildcards ? and * in the filename. *** PARAMETER NOT CURRENTLY IMPLEMENTED ***
;
; Returns:
;
;   - EAX                        Return value as defined in ZL_Extract Status, Error & Return Values
;   - EBX                        Count of processed files - files extracted (and succesfully written out) or listed, that matched filespec.
;
;
; Example Usage:
;
;   .data
;   MyZipFile    db 'BigZipFile001.zip', 0
;   MyDocsFolder db 'C:\My Documents', 0
;   DocFilesOnly db '*.doc', 0
;   AllFiles     db '*.*', 0
;
;   .code 
;   Invoke ZL_Extract, NULL, Addr MyZipFile, NULL, Addr MyDocsFolder, NULL               ; extract all files from BigZipFile001.zip to C:\My Documents
;   Invoke ZL_Extract, NULL, Addr MyZipFile, Addr DocFilesOnly, Addr MyDocsFolder, NULL  ; extract all .doc files from BigZipFile001.zip to C:\My Documents
;   Invoke ZL_Extract, NULL, Addr MyZipFile, Addr AllFiles, NULL, NULL                   ; extract all files (*.*) files from BigZipFile001.zip to current folder
;   
; 
; Advanced Usage with ZL_SetProcessInfoCallback:
;
;   MyZLCallbackProc PROTO :DWORD, :DWORD, :DWORD, :DWORD   
;
;   .data
;   MyZipFile    db 'BigZipFile001.zip', 0
;
;   .code
;   Invoke ZL_SetProcessInfoCallback, Addr MyZLCallbackProc                              ; Setup callback proc MyZLCallbackProc
;   Invoke ZL_Extract, ZLE_MODE_LIST, Addr MyZipFile, NULL, NULL, NULL                   ; lists all files from BigZipFile001.zip and sends file info to callback proc
;
;   MyZLCallbackProc PROC lpZipArchiveInfo:DWORD, lpProcessFileInfo:DWORD, lpFileInfo:DWORD, dwStatus
;       .IF dwStatus == ZLE_STATUS_LISTING_FILES
;           mov ebx, lpProcessFileInfo
;           lea esi, [ebx].ZL_PROCESSFILEINFO.szFileName ; get filename in esi, to copy somewhere or something
;           mov ebx, lpFileInfo
;           mov eax, [ebx].ZL_FILEINFO.uncompressed_size ; get uncompressed size of file
;           .
;           .
;           .
;       .ENDIF
;       ret
;   MyZLCallbackProc ENDP
;
;-----------------------------------------------------------------------------------------------------------------------------------------------------------------

;-----------------------------------------------------------------------------------------------------------------------------------------------------------------
;
; ZL_SetProcessInfoCallback      Set a user defined callback procedure to receive information about a zip archive and its files as they are extracted/listed.
;
; Parameters:
;
;   - lpdwCallbackProc           Address of user defined procedure to receive information from ZL_Extract as it processes files. 
;
; Returns:
;
;   - EAX                        Contains TRUE
;
;
; Example Usage: 
;
;   Invoke ZL_SetProcessInfoCallback, Addr MyZLCallbackProc ; example of setting callback proc to user defined MyZLCallbackProc function
;
;   Example of PROTO defined in YOUR code to receive info as file is processed (extracted/listed etc):
;
;   MyZLCallbackProc PROTO :DWORD, :DWORD, :DWORD ; example definition of user defined callback function
;
;   Example of used defined PROC and param received:
;
;   MyZLCallbackProc PROC lpZipArchiveInfo:DWORD, lpProcessFileInfo:DWORD, lpFileInfoStruct:DWORD, dwStatus:DWORD 
;  
;     - lpZipArchiveInfo is a pointer to ZL_ZIPARCHIVEINFO structure
;     - lpProcessFileInfo is pointer to ZL_PROCESSFILEINFO structure
;     - lpFileInfo is pointer to ZL_FILEINFO structure.
;     - dwStatus is status code as defined below in ZL_Extract Status, Error & Return Values:
;       < 0 is error code, > 0 is status code, 0 is finished/success 
;
;
; Advanced Usage with ZL_Extract:
;
;   MyZLCallbackProc PROTO :DWORD, :DWORD, :DWORD, :DWORD   
;
;   .data
;   MyZipFile    db 'BigZipFile001.zip', 0
;
;   .code
;   Invoke ZL_SetProcessInfoCallback, Addr MyZLCallbackProc                              ; Setup callback proc MyZLCallbackProc
;   Invoke ZL_Extract, NULL, Addr MyZipFile, NULL, NULL, NULL                            ; extract all files from BigZipFile001.zip and sends file info to callback proc
;
;   MyZLCallbackProc PROC lpZipArchiveInfo:DWORD, lpProcessFileInfo:DWORD, lpFileInfo:DWORD, dwStatus
;       .IF dwStatus == ZLE_STATUS_PROCESSING
;           mov ebx, lpProcessFileInfo
;           mov eax, [ebx].ZL_PROCESSFILEINFO.dwPercentageProcessed ; get percentage processed, maybe convert to text and display in a text label?
;           mov ebx, lpFileInfo
;           mov eax, [ebx].ZL_FILEINFO.uncompressed_size ; get uncompressed size of file
;           mov ebx, lpFileInfo
;           mov eax, [ebx].ZL_FILEINFO.compressed_size ; get compressed size of file
;           
;           ; Display progress info, for example a progress bar. Then do something else.
;
;       .ELSEIF sdword ptr dwStatus < 0 ; some error occured, alert user.
;           Invoke MessageBox, NULL, Addr szOhDearAnErrorOccured, Addr szErrorTitle, MB_OK
;       .ENDIF    
;       ret
;   MyZLCallbackProc ENDP
;
;-----------------------------------------------------------------------------------------------------------------------------------------------------------------


ZL_Extract                      PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD ; dwOptions, lpszZipFile, lpszOutputFolder, lpszFileSpec, lpszExcludeFileSpec
ZL_SetProcessInfoCallback       PROTO :DWORD ; Addr of callback proc


; Structure of zip archive info passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
ZL_ZIPARCHIVEINFO               STRUCT
    dwTotalFiles                DD 0 ; total files in zip archive
    dwTotalFolders              DD 0 ; total folders in zip archive
    dwTotalFilesCompressedSize  DD 0 ; total sizes of all files (Compressed) in zip archive
    dwTotalFilesUncompressedSize DD 0; total sizes of all files (uncompressed) in zip archive
ZL_ZIPARCHIVEINFO               ENDS

; Structure of internal file processing info passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
ZL_PROCESSFILEINFO              STRUCT
    dwFileNo                    DD 0 ; file number processed
    szFullPathFilename          DB MAX_PATH dup (0) ; full path and filename
    szFileName                  DB MAX_PATH dup (0) ; just filename and extension
    dwBytesProcessed            DD 0 ; bytes processed (written) 
    dwPercentageProcessed       DD 0 ; calculated percentage
ZL_PROCESSFILEINFO              ENDS

; Structure of internal file info time (sub struct of ZL_FILEINFO) passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
ZL_FILETIMEINFO                 STRUCT
    tm_sec		                DD 0
    tm_min		                DD 0
    tm_hour		                DD 0
    tm_mday		                DD 0
    tm_mon		                DD 0
    tm_year		                DD 0
ZL_FILETIMEINFO		            ENDS

; Structure of internal file info passed back to user defined callback function and set using ZL_SetProcessInfoCallback function
ZL_FILEINFO  		            STRUCT
    version			            DD 0
    version_needed	            DD 0
    flag			            DD 0 
    compression_method	        DD 0
    dosDate				        DD 0
    crc					        DD 0
    compressed_size		        DD 0
    uncompressed_size	        DD 0
    size_filename		        DD 0
    size_file_extra		        DD 0
    size_file_comment	        DD 0
    disk_num_start		        DD 0
    internal_fa			        DD 0
    external_fa			        DD 0 
    tmu_date			        ZL_FILETIMEINFO <>
ZL_FILEINFO		                ENDS


.CONST
; ZLE_MODE constants. ZL_Extract values for dwOptions paramter:
ZLE_MODE_EXTRACT                EQU  0 ; Extract files from lpszZipFile that match lpszFileSpec from lpszZipFile to lpszOutputFolder
ZLE_MODE_LIST                   EQU  1 ; List files from lpszZipFile that match lpszFileSpec. Other params are ignored
ZLE_MODE_INFO                   EQU  2 ; Information about lpszZipFile as stored in ZL_ZIPARCHIVEINFO. Other params are ignored

; ZL_Extract Status, Error & Return Values
ZLE_STATUS_BEGIN_PROCESSING     EQU  5 ; Start processing of a file in the zip archive: extracting/listing etc
ZLE_STATUS_PROCESSING           EQU  4 ; Actual processing of file. Passed param has details of file, bytes etc
ZLE_STATUS_FINISH_PROCESSING    EQU  3 ; Finished processing of file in zip archive.
ZLE_STATUS_LISTING_FILES        EQU  2 ; Listing files only, no extraction
ZLE_STATUS_ARCHIVE_INFO         EQU  1 ; Info about archive only, no extraction
ZLE_STATUS_SUCCESS              EQU  0 ; Extracted file(s) succesfully or finished listing files
ZLE_ERR_NO_ZIP                  EQU -1 ; No zip archive specified
ZLE_ERR_INVALID_ZIP             EQU -2 ; Invalid zip archive
ZLE_ERR_INTERNAL_INFO           EQU -3 ; Failed to get internal zip file info
ZLE_ERR_OPEN_FILE_FAILURE       EQU -4 ; Failed to open internal zip file
ZLE_ERR_ALLOC_MEMORY            EQU -5 ; Failed to allocate memory for unzip operation
ZLE_ERR_READ_DATA_FAILURE       EQU -6 ; Failed to read internal zip file
ZLE_ERR_CREATE_FILE_FAILURE     EQU -7 ; failed to create extracted file
ZLE_ERR_WRITE_DATA_FAILURE      EQU -8 ; Failed to write data to extracted file
ZLE_ERR_NO_MATCHING_FILES_FOUND EQU -9 ; No files matching filespec where found

